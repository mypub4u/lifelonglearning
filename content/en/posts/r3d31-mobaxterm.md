---
title: "R3:D32 - mobaXterm"
date: 2020-03-12T02:35:51+09:00
description: MobaXterm - my central control center
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - mobaXterm
  - ssh
  - rdp
  - xwindows
  - x11
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/moba4me.png
meta_image: images/r3/moba4me.png
---

#### R3:D31 - MobaXterm

<br/>

![](/lifelonglearning/images/r3/moba4me.png)

Today I finally installed my beloved Mobaxterm on my rog. Imagine what I have discovered: All of my WLS installations are displayed directly. And then just started cmd, bash and powershell ... No shell wish remains open. Just imagine Putty on Steroids.

<br/>

#### Sessions

![](/lifelonglearning/images/r3/moba1.jpg)
Just check all the Type of Sessions you can connect to:

1. SSH
1. Telnet
1. Rsh
1. RDP
1. VNC
1. FTP
1. SFTP
1. Serial
1. File
1. Shell
1. Browser
1. Mosh
1. AWS S3
1. WSL

<br/>

#### Server

![](/lifelonglearning/images/r3/moba2.jpg)

And if this is not enough. If you want to start a Server on the fly, beside X-Server, which is per default enabled, check this list:

1. TFTP
1. FTP
1. HTTP
1. SSH/SFTP
1. Telnet
1. NFS
1. VNC
1. Cron
1. lperf

<br/>

#### Multi-Execution Mode

![](/lifelonglearning/images/r3/moba3.jpg)

Did you ever see a Multi-Execution Mode for up to 12 Sessions for free? You didn't or did you? Tell me!

<br/>

#### Free for up to 12 Sessions

![](/lifelonglearning/images/r3/moba4.jpg)

Yes, it's free for up to 12 Sessions. And I really, really think about buying the pro version for just \$49.

So just download it, play with it, and if you need it, buy it: [moba homepage](https://mobaxterm.mobatek.net/)

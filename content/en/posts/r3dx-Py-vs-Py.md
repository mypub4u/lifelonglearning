---
title: "R3:D7 - Py Vs Py"
date: 2020-02-16T19:23:51+09:00
description: Python, Jython, Groovy, ...
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - groovy
  - jython
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPi-Logo-And-Walleve.png
meta_image: images/r3/PyPi-Logo-And-Walleve.png
---

![](/lifelonglearning/images/r3/PyPi-Logo-And-Walleve.png)

I'm a groovian, most likely. So a lot of python syntax-estetics are in my veins.

I admit learning python should also help me write better groovy code. Just like R helped me think functionally.

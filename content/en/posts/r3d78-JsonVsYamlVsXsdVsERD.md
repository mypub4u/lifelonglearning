---
title: "R3:D79 - From ERD/XSD to Json & Yaml"
date: 2020-05-05T00:45:51+09:00
description: Data Markup with JSON or Yaml perhaps instead of XSD and ERD.
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - javascript
  - json
  - yaml
  - vscode
  - chrome
  - firefox
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/JsonVsYaml.png
meta_image: images/r3/JsonVsYaml.png
---

### From ERD/XSD to Json & Yaml

<br/>

![](/lifelonglearning/images/r3/JsonVsYaml.png)

### Drag'n Drop MvP
My litle Drag'n Drop Project "DJ Masterpieces" was just a quick and dirty HTML Hack. Repeating data & HTML in one file. Nightmare to maintain. 

### XSD to Vue-Components
Then I discovered the beauty of Vue-Components. I quickly developed a complex tag-structure that quickly reminded me of the design with XSD. Why not develop a XSD to a Vue-Component generator? But that's just only the structure, the data is not yet managed.

### ERD vs. YAML/JSON
So I started to think about the data structure. In the last years that was always the point where I installed an ERD tool and developed beautiful fast growing data models. But this time I wanted to do it differently. 

### Yaml vs. JSON
I started writing directly in JSON and tried out one or the other editor. The "jsoneditoronline" is a real cool one. It's a bit like codepen or jsfiddle for json files. You can save it online (1) or create share-url (2). It is possible to write in the left pane (3) and navigate in a tree-view (4). Or diff (5) two json files. Great Stuff!

![](/lifelonglearning/images/r3/json-online-editor.png)


+ [JSON Editor Online](https://jsoneditoronline.org/#left=local.mijude&right=local.rilefu)

<br/>

Although JSON is in use, e.g. is extremely convenient by jq cli-tool or the convenient dot notation, it is an abomination to write. YAML is like a markdown for writing. Very simple, very modest in the possibilities. And I tried different editors here too. I noticed jsonvsyaml. This allows me to write in YAML in real time and see directly how it works in JSON. 

![](/lifelonglearning/images/r3/json2yaml.png)


+ [Json2Yaml Online](https://www.json2yaml.com/)


### Data-Management vs. HTML-Hack
So my plan is a bit like this blog writing-flow itself 

```Flow
Markdown > hugo > webpage
```
<br/>

Writing/Collecting data as markdown'ish as possible. This seams imho YAML. Then generate all further Stuff from there.

```Flow
YAML -> JSON -> Go, Python, JS, Swagger ...
```
<br/>

### Paste From JSON
And here a VS-Code Plugin comes into place: "Paste From JSON" a match made in heaven. Write/Generate your JSON than let the Plugin do the ugly boilerplate for you. I love this approach.

![](/lifelonglearning/images/r3/demo-past-from-json.gif)

+ [Paste JSON as Code for VS, Github)](https://github.com/quicktype/quicktype-vscode)
+ [Paste JSON as Code for VS, Marketplace](https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype)

<br/>
Also for Visual Studio (not VS Code):

+ [Paste JSON as Code for VS, Github)](https://github.com/quicktype/quicktype-vs)
+ [Paste JSON as Code for VS, Marketplace](https://marketplace.visualstudio.com/items?itemName=typeguard.quicktype-vs)






<br/>
<br/>

### Some further Tools:
<br/>


#### Chrome Addons
+ [Firefox JSON-Editor Plugin](https://addons.mozilla.org/de/firefox/addon/json-beautifier-editor/?src=search)
+ [Firefox JSON-Editor Plugin, Github](https://github.com/webpatch/JSON-Editor-Extension)
+ [Google Chrome Editor](https://chrome.google.com/webstore/detail/json-editor/lhkmoheomjbkfloacpgllgjcamhihfaj?hl=de)


#### VS Code Plugins:
+ [Yaml by RedHat](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml)
+ [Json for Visual Studio Code by ZaiChen, Github](https://github.com/ZainChen/vscode-json)
+ [Json for Visual Studio Code, Marketplace](https://marketplace.visualstudio.com/items?itemName=ZainChen.json)

---
title: "R3:D58to60 - DragAndDrop"
date: 2020-04-10T23:22:51+09:00
description: Drag And Drop Solutions 
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - javascript
  - vuejs
  - react
  - html5
  - jquery
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/FlaskOnDocker.png
meta_image: images/r3/FlaskOnDocker.png
---

#### R3:D55 - Flask On Docker

<br/>

![](/lifelonglearning/images/r3/FlaskOnDocker.png)

Yesterday I've starting to analyze different drag and drop solutions especially for list items from react, vue, native html5, jquery and more. Codepen was a good help, just to inspect and compare the differen approaches.

My requierements are:
- simple as possible
- serializable
- small footprint
- minimal boileplate
- works on touch-devices

After analyzing a lot of solutions in different frameworks I found. 

<br/>

<br/>

---
title: "R1:D01 - Welcome ..."
date: 2020-02-10T06:40:51+09:00
description: ... to round 3 on my 100DaysOfCode Series
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
authorImage: "https://pbs.twimg.com/profile_images/941863772762918912/rk5gBvU9_400x400.jpg"
tags:
  - r3
  - blog
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/R3-Logo-01.png
meta_image: images/r3/R3-Logo-01.png
---

# ... to Round 3 on my 100DaysOfCode.

![](/lifelonglearning/images/r3/R3-Logo-01.png)

## Targets

Learning, Coding, Blogging.

My Learning Curriculum is just a collection to give my 100 days a direction. My Coding is like making music, music that makes you shake, with deep basses. And I'm lazy; means my core motivation for coding is to "automate the boring stuff". Blogging is like breating. Years ago I've started blogs about Music, Music-Gadgets, SCA and more. Now I try not to do another specialized blog, but an all purpose blog around my central approach: "LifeLongLearning". The 100-doc-r3 is a vehicel to bring me back on track for a regulary blogging cadence.

## Setup

Hugo, Gitlab, Twitter, GitJournal, Mastodon, Gists, Snippet,
VsCode, WebIDE's, Jupyter ...

Beside all my labs, aka Learning-Repos on github and gitlab, I wanna have a markdown-centric weblog setup with a smart & fancy static site generator around. I really love vue and vuepress, but I have a love-hate relationship to npm... So finally hugo with the zzo Setup won the race.

Because I don't like zuckerbergs facebook, instagramm or whatsapp and I'm on search on more alternatives. Fediverse a network of micro-blogging systems sounds nice. Also telegram or discord makes me curious to use more than a gaming-sidekick. Anyway here comes my first project ideas: For twitter/youtube I wanna have better list / abo management. For fediverse I think about a bridge to telegram ... Soo many ideas ...

## Curriculum

- Python
  - serverless
  - Raspery PI
  - tweepy
  - rbt
  - jupyter / sagemaker / paws
  - SPA with django & vue
- FOSS RPA (Robotic Process Automation)
- Ubuntu Touch on my mobile
- More Beats, Kibana, Elastic, Grafana, StatsD = MoLo in my vein
- MarineCo2
- ... one last thing

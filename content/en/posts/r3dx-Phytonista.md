---
title: "R3:D5 - Pythonist or Pythoneer"
date: 2020-02-14T19:23:51+09:00
description: Socialising while learning
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - phytonista
  - socialcoding
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPi-Logo-And-Walleve.png
meta_image: images/r3/PyPi-Logo-And-Walleve.png
---

![](/lifelonglearning/images/r3/PyPi-Logo-And-Walleve.png)

Coding alone is like playing football alone. It's more fun to compute together.

Your HackerSpaces & Labs around you.

Meetup.com

https://www.meetup.com/de-DE/pro/pydata/

Twitter-Listen
PhytonistaCafe
https://www.pythonistacafe.com/
with this nice pick:
https://realpython.com/python-beginner-tips/#tip-1-code-everyday

freecodecamp
https://guide.freecodecamp.org/python

https://pythondiscord.com/

The good old BBS-Forum:
https://www.python-forum.de/

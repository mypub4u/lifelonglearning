---
title: "R3:D5to7 - twypy"
date: 2020-02-16T17:23:51+09:00
description: Using Tweepy for Twitter API
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - twitter
  - tweepy
  - module
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/twypy.png
meta_image: images/r3/twypy.png
---

![](/lifelonglearning/images/r3/twypy.png)

One of my main reason to digg deeper into python is tweepy. In a little SubSession at Weekend I made my fingers wet:

### Setup a Twitter Dev-Account

Amazing how many questions Twitter asks and how much they want to know. After a lot of Formulas and an Email-Verification your in.

### Setup an App

Next Step is to setup a Twitter Application. For this you need an URL.

### Create a Repo/Lab

Because of needing an URL, and perhaps later reuse I have setup a gitlab repo.

### Get API-Tokens

The last step before coding is to generate API-Keys. There four of theme:

1. UserAccountToken
1. UserCountSecretToken
1. ApplicationToken
1. ApplicationSecretToken

The Application Tokens you see only once.

### tweepy first touch

After Setting up I tried a first timeline scrap. Yes seems realy cool.

### tweepy dig deeper

Today I've tried a lot of tweepy calls, and I'm really happy. If I have a better grasp in Python I definitely want to help reduce the issues and possibly the PRs. For now I would analyse more Python Twitter-API's.

<br/>

### Some tweepy specific Links

1. [tweepy Repo](https://github.com/tweepy)
1. [tweepy HP](http://www.tweepy.org/)
1. [tweepy Docs](http://docs.tweepy.org/en/latest/)
1. [tweepy Discord](https://discord.gg/bJvqnhg)
1. [tweepy Stackoverflow](https://stackoverflow.com/questions/tagged/tweepy)
1. [tweepy Guide by Richard Chadwick](https://towardsdatascience.com/tweepy-for-beginners-24baf21f2c25)
1. [tweepy Guide by Tara Boyle](https://towardsdatascience.com/my-first-twitter-app-1115a327349e)
1. [tweepy Guide by Anthony Sistilli](https://www.deepcoredata.com/twitter-data-mining-guide-python/)

<br/>

### Some other "official" listed py libs

- [python-twitter](http://python-twitter.readthedocs.io/en/latest/) on [github](https://github.com/bear/python-twitter) by the Python-Twitter Developers
- [ptt](https://mike.verdone.ca/twitter/) on [github](https://github.com/sixohsix/twitter) by the Python Twitter Tools developer team
- [TwitterSearch](https://github.com/ckoepp) on [github](https://github.com/ckoepp/TwitterSearch) by @ckoepp
- [twython](https://stackoverflow.com/questions/tagged/twython) on [github](https://github.com/ryanmcgrath/twython) by [@ryanmcgrath](https://twitter.com/ryanmcgrath) and [@mikehelmick](https://twitter.com/mikehelmick)
- [TwitterAPI](https://pypi.org/project/TwitterAPI/) on [github](https://github.com/geduldig/TwitterAPI) by [@geduldig](https://github.com/geduldig)

<br/>

### SDK listed @programmableweb

1. [Twitter Python SDK by Ryan McGrath](https://www.programmableweb.com/sdk/twitter-python-sdk-ryan-mcgrath) -|- [twython - git](https://github.com/ryanmcgrath/twython)
1. [Twitter Python SDK by Vinta Software](https://www.programmableweb.com/sdk/twitter-python-sdk-vinta-software) -|- [tapioca-git](https://github.com/vintasoftware/tapioca-twitter)
1. [Twitter Python SDK by odrling](https://www.programmableweb.com/sdk/twitter-python-sdk-odrling) -|- [peony-git](https://github.com/odrling/peony-twitter)
1. [Twitter Python SDK by tweepy](https://www.programmableweb.com/sdk/twitter-python-sdk-tweepy) -|- [tweepy-git](https://github.com/tweepy/tweepy)
1. [Twitter Python SDK by geduldig](https://www.programmableweb.com/sdk/twitter-python-sdk-geduldig) -|- [Twitter-git](https://github.com/geduldig/TwitterAPI)
1. [Twitter Python SDK by Mike Verdone](https://www.programmableweb.com/sdk/twitter-python-sdk-mike-verdone) -|- [ptt-git](https://github.com/sixohsix/twitter)
1. [Twitter Python SDK by Mike Taylor](https://www.programmableweb.com/sdk/twitter-python-sdk-mike-taylor) -|- [bear-python-twitter-git](https://github.com/bear/python-twitter)

<br/>

### Some more not soo offical:

- [Twint PyPi](https://pypi.org/project/twint/)
- [Twint Doku Wiki](https://github.com/twintproject/twint/wiki)
- [Twint Repo](https://github.com/twintproject/twint)
- [Twint Guide from OSINT](https://www.hackers-arise.com/post/2019/05/03/open-source-intelligence-osint-part-1-mining-intelligence-from-twitter-mattgaetz)

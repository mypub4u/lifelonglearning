---
title: "R3:D2 - Zen of Python"
date: 2020-02-11T19:40:51+09:00
description: 19 Python Statements to think about
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/Zen-Of-Python.png
meta_image: images/r3/Zen-Of-Python.png
---

Choosing a language alone makes you not a cool coder. Coding guidelines can help a little. But how about a coding philosophy? Not only 4 py.

![](/lifelonglearning/images/r3/Zen-Of-Python.png)

```python
print("Oohhm")
```

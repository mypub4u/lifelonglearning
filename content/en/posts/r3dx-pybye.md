---
title: "R3:D8 - PyByE"
date: 2020-02-17T19:23:51+09:00
description: Python By Examples
draft: true
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - groovy
  - jython
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPi-Logo-And-Walleve.png
meta_image: images/r3/PyPi-Logo-And-Walleve.png
---

![](/lifelonglearning/images/r3/PyPi-Logo-And-Walleve.png)

Learning by coding is for the basic stuff. Learning by reading and adopting is my fastlane to more complex stuff. And by the way after gobeye this is my path to greate python sourcecode corpus.

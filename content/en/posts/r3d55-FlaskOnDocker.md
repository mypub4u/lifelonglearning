---
title: "R3:D55 - FlaskOnDocker"
date: 2020-04-07T22:19:51+09:00
description: Flask Apps inside Docker
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - flask
  - python
  - docker
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/FlaskOnDocker.png
meta_image: images/r3/FlaskOnDocker.png
---

#### R3:D55 - Flask On Docker

<br/>

![](/lifelonglearning/images/r3/FlaskOnDocker.png)

I have checked a lot of Docker Images on DockerHub to find some good examples for learning how to build simple and fast flask apps and microservice. Here are my findings:

<br/>

#### flask-hello-world

A simple Hello World in a Flask inside a Docker. Great starting point from scratch:
https://hub.docker.com/r/tullyrankin/flask-hello-world

<br/>

#### flask-example

Another simple example working ootb.
https://hub.docker.com/r/macinv/flask-example

<br/>

#### flask-restplus-server-example

Great if you wanna start with an OpenApi / Swagger centric Microservice
https://hub.docker.com/r/frolvlad/flask-restplus-server-example

<br/>

#### flask-formula

I love this "docker-compose up -d" and you have full blown stack installed, with just a single cli command. Awesome!
https://hub.docker.com/r/trydirect/flask-formula

![](/lifelonglearning/images/r3/FullstackFlask.png)

<br/>

#### ecs tutorial

Find here a nice tutorial of a flask centric full stack deployment on aws ecs.
https://hub.docker.com/r/chris24walsh/flask-aws-tutorial

<br/>

#### Some More Things

- [Flask-Homepage](https://flask.palletsprojects.com/en/1.1.x/)
- [awesome-junliu](https://github.com/JunliuHub/awesome-flask)
- [awesome-somma](https://github.com/somma/awesome-flask)
- [awesome-humiaozuzu](https://github.com/humiaozuzu/awesome-flask)
- [awesome-mjhea0](https://github.com/mjhea0/awesome-flask)
- [youtube-tutorial (de)](https://www.youtube.com/watch?v=EGYpNQ3GL1Y&list=PLNmsVeXQZj7otfP2zTa8AIiNIWVg0BRqs)

<br/>

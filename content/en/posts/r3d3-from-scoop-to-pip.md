---
title: "R3:D3 - From scoop to pip"
date: 2020-02-12T19:15:51+09:00
description: Setup, Config & Package Manager
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - scoop
  - pip
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/PyPi-Logo-And-Walleve-wide.png
---

![](/lifelonglearning/images/r3/PyPi-Logo-And-Walleve.png)

If you are programming or not to have a handy Package-Manager at your fingertips, gives you rocket sience fast. On Linux it's daylie business with apt, dpkg, rpm and many more. On Mac you know perhaps homebrew. This one is git based. But on Windows? Chocolatery seems nice but too complex. So I found scoop.sh a match made in heaven. Simple, clean and github based.

Installing Python is just a

```
scoop intall python
```

The most programming languages comes with at least one packagemanager for it's own modules and packages. For Python it is pip. And happily since Python 3.4 and 2.7 it's just installed with python itself. The Repository behind is called PyPI (= Python Package Index)

Try it for your self with

```
pip
pip -v
pip search tweepy
```

If you know the package you want everything is fine. But what if you wanna search for a solution and you did not know the concrete module or package?

Here some helpfull links:

- https://pypi.org/search/
- https://libraries.io/pypi

<br/>
Understanding PyPi itself:

- https://packaging.python.org/
- https://guide.freecodecamp.org/python/using-pip
- https://en.wikipedia.org/wiki/Python_Package_Index

<br/>
And here are the complete Topic-Tree of PyPi with 24 Top-Topics and 254 entries at all:

```python
Adaptive Technologies
Artistic Software
Communications
    BBS
    Chat
        Internet Relay Chat
    Conferencing
    Email
        Address Book
        Email Clients (MUA)
        Filters
        Mailing List Servers
        Mail Transport Agents
        Post-Office
            IMAP
            POP3
    Fax
    File Sharing
        Gnutella
    Ham Radio
    Internet Phone
    Telephony
    Usenet News
Database
    Database Engines/Servers
    Front-Ends
Desktop Environment
    File Managers
    Gnome
    GNUstep
    K Desktop Environment (KDE)
    Screen Savers
    Window Managers
        Fluxbox
        XFCE
Documentation
    Sphinx
Education
    Computer Aided Instruction (CAI)
    Testing
Games/Entertainment
    Arcade
    Board Games
    First Person Shooters
    Fortune Cookies
    Multi-User Dungeons (MUD)
    Puzzle Games
    Real Time Strategy
    Role-Playing
    Side-Scrolling/Arcade Games
    Simulation
    Turn Based Strategy
Home Automation
Internet
    File Transfer Protocol (FTP)
    Finger
    Log Analysis
    Name Service (DNS)
    Proxy Servers
    WAP
    WWW/HTTP
        Browsers
        Dynamic Content
            CGI Tools/Libraries
            Content Management System
            Message Boards
            News/Diary
            Page Counters
            Wiki
        HTTP Servers
        Indexing/Search
        Session
        Site Management
            Link Checking
        WSGI
            Application
            Middleware
            Server
    XMPP
    Z39.50
Multimedia
    Graphics
        3D Modeling
        3D Rendering
        Capture
            Digital Camera
            Scanners
            Screen Capture
        Editors
            Raster-Based
            Vector-Based
        Graphics Conversion
        Presentation
        Viewers
    Sound/Audio
        Analysis
        Capture/Recording
        CD Audio
            CD Playing
            CD Ripping
            CD Writing
        Conversion
        Editors
        MIDI
        Mixers
        Players
            MP3
        Sound Synthesis
        Speech
    Video
        Capture
        Conversion
        Display
        Non-Linear Editor
Office/Business
    Financial
        Accounting
        Investment
        Point-Of-Sale
        Spreadsheet
    Groupware
    News/Diary
    Office Suites
    Scheduling
Other/Nonlisted Topic
Printing
Religion
Scientific/Engineering
    Artificial Intelligence
    Artificial Life
    Astronomy
    Atmospheric Science
    Bio-Informatics
    Chemistry
    Electronic Design Automation (EDA)
    GIS
    Human Machine Interfaces
    Hydrology
    Image Recognition
    Information Analysis
    Interface Engine/Protocol Translator
    Mathematics
    Medical Science Apps.
    Physics
    Visualization
Security
    Cryptography
Sociology
    Genealogy
    History
Software Development
    Assemblers
    Bug Tracking
    Build Tools
    Code Generators
    Compilers
    Debuggers
    Disassemblers
    Documentation
    Embedded Systems
    Internationalization
    Interpreters
    Libraries
        Application Frameworks
        Java Libraries
        Perl Modules
        PHP Classes
        Pike Modules
        pygame
        Python Modules
        Ruby Modules
        Tcl Extensions
    Localization
    Object Brokering
        CORBA
    Pre-processors
    Quality Assurance
    Testing
        Acceptance
        BDD
        Mocking
        Traffic Generation
        Unit
    User Interfaces
    Version Control
        Bazaar
        CVS
        Git
        Mercurial
        RCS
        SCCS
    Widget Sets
System
    Archiving
        Backup
        Compression
        Mirroring
        Packaging
    Benchmark
    Boot
        Init
    Clustering
    Console Fonts
    Distributed Computing
    Emulators
    Filesystems
    Hardware
        Hardware Drivers
        Mainframes
        Symmetric Multi-processing
    Installation/Setup
    Logging
    Monitoring
    Networking
        Firewalls
        Monitoring
            Hardware Watchdog
        Time Synchronization
    Operating System
    Operating System Kernels
        BSD
        GNU Hurd
        Linux
    Power (UPS)
    Recovery Tools
    Shells
    Software Distribution
    Systems Administration
        Authentication/Directory
            LDAP
    System Shells
Terminals
    Serial
    Telnet
    Terminal Emulators/X Terminals
Text Editors
    Documentation
    Emacs
    Integrated Development Environments (IDE)
    Text Processing
    Word Processors
Text Processing
    Filters
    Fonts
    General
    Indexing
    Linguistic
    Markup
        HTML
        LaTeX
        SGML
        VRML
        XML
Utilities

```

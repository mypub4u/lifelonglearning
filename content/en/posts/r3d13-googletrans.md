---
title: "R3:D13 - googletrans"
date: 2020-02-22T23:35:51+09:00
description: Translate API for Python
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - python
  - googletranslate
  - module
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/MeineGedanken.png
meta_image: images/r3/MeineGedanken.png
---

#### Google Translate

<br/>

![](/lifelonglearning/images/r3/MeineGedanken.png)

Yesterday and the day before yesterday I was not able to code, because of that incredible assassination in #hanau remember me about #halle, #lübcke and more.
Today I want just to say:

My thoughts are with you!

But in the languages spoken by the victims. As far as I know, the following languages were included:

- Kurmandschi, ku
- Turkish, tr
- German, de
- Romanian, ro
- Bulgarian, bg
- hebrew, he
- Bosnian, bs
- Afgahn (Pashto, pt; fa, persian)

So I'm searching a python module, and wrote a script for translation.

```python
pip install googletrans
```

```python
#!/usr/bin/env python
from googletrans import Translator

translator = Translator()

mydict = {
  "English": "en",
  "Kurmandschi": "ku",
  "Turkish": "tr",
  "German": "de",
  "Romanian": "ro",
  "Bulgarian": "bg",
  "hebrew": "he",
  "Bosnian": "bs",
  "Pashto": "pt",
  "Persian": "fa"
}

for mylang, myshort in mydict.items():
  translated = translator.translate('Meine Gedanken sind bei Euch!', src='de', dest=myshort)
  print(mylang + ": \t" + translated.text)

```

More Links for googletrans:

- (http://zetcode.com/python/googletrans/)
- (https://py-googletrans.readthedocs.io/en/latest/)

Other Python Bindings / API's for google translate:

- (https://pypi.org/project/translate/)
- (https://pythonhosted.org/goslate/)

---
title: "R3:D60 - Code Playgrounds"
date: 2020-04-12T22:22:51+09:00
description: Some Playgrounds for Social Coding
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
author: cF
authorEmoji: 🚀
tags:
  - r3
  - javascript
  - playgrounds
  - jsfiddle
  - jsbin
  - codepen
series:
  - 100-doc-r3
categories:
  - 100DaysOfCode
image: images/r3/playgrounds.png
meta_image: images/r3/playgrounds.png
---

#### Code Playgrounds

<br/>

![](/lifelonglearning/images/r3/playgrounds.png)

Code Playgrounds are in the core of social coding. You can show, share, explain or ask with the help of

+ JSBin
+ JSFiddle
+ CodePen
 

It's also helpfull to know 1-2 of this playgrounds if you have questions on stackoverflow to show what's your problem.

<br/>

#### JSFiddle

![](/lifelonglearning/images/r3/jsfiddle.png)

JSFiddle did not have an own "Search Engine", but you can use google for e.g. with "site:jsfiddle.net [searchterm]"

[Startpage](https://jsfiddle.net/)
[Documenation](https://docs.jsfiddle.net/)
Search in Google: site:jsfiddle.net [searchterm]
[How to Stackoverflow](https://meta.stackoverflow.com/questions/358212/how-do-i-include-code-for-jsfiddle)
[Newest Questions](https://stackoverflow.com/questions/tagged/jsfiddle)


<br/>

#### JSBin

![](/lifelonglearning/images/r3/jsbin.svg)

JS Bin is an open source collaborative web development debugging tool. Made in Brighton, England with blood sweat and code. Ah Brighton, where so many DnB sounds come from. Nice is the integration with gist. Searching via google sitesearch is not that cool, but I did not have found something better.

[Startpage](https://jsbin.com/?html,css,js,output)
[Documenation](https://jsbin.com/help/)
Search in Google: site:jsbin.com [searchterm]
[Newest Stackoverflow Questions](https://stackoverflow.com/questions/tagged/jsbin)

<br/>

#### Codepen

![](/lifelonglearning/images/r3/codepen.svg)

CodePen is a website hosting runnable HTML/CSS/JS code snippets for testing and sharing. It has a build-in Search-Engine. And here you can find a lot of stunning stuff. It feel's like a Demo-Scene Platform for HTML based snippets.

[Startpage](https://codepen.io/)
[Documenation](https://blog.codepen.io/documentation/)
[Search: InSite or via site:codepen.io [searchterm]
[Newest Stackoverflow Questions](https://stackoverflow.com/questions/tagged/codepen)

<br/>

#### More Resources
+ [20 Alternatives to codepen](https://www.slant.co/options/5535/alternatives/~codepen-alternatives)
+ [A Round up of Online Code Playgrounds](https://www.sitepoint.com/round-up-online-code-playgrounds/)




+++
title = "Über dies"
description = "Mein lebenlanges Lernen"
type = "about"
date = "2020-02-20"
+++

... tbd ...

Bevor hier jetzt was so richtig schlaues her kommt, werde ich erstmal sammeln, was ich so am zzo Setup anpassen möchte:

- Prominenterer Sprachumschalter
- Schrift (Weniger Fett FireCode)
- Vorhandene Posts > Archive (via 2019)
- Runde Tag/Cat-Labels (tief in's CSS rein)
- Bilder rechts, statt links. Dann ist's auch mal OK keine Bilder zu haben
- Categories over Tags

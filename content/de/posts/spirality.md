---
title: "Spirality"
date: 2020-02-16T12:00:06+09:00
description: "Digitaler Spirograph - Wunderbar"
draft: false
enableToc: false
enableTocContent: false
tags:
  -
series:
  -
categories:
  - math
libraries:
  - katex
image: images/2020/spirality.jpg
---

Da hab ich heute meinen neuen Surface Pen ausgepackt und wollte mal nach ein paar netten Apps für Surface & Pen suchen, und hab das hier gefunden:

[[Spirality](/lifelonglearning/images/2020/spirality.jpg)](https://twitter.com/SpiralityApp)

Hier dazu ein Beispiel ...

... als PNG:
![](/lifelonglearning/images/2020/Spiral002.png)

... als Animated GIF:
![](/lifelonglearning/images/2020/Spiral002.gif)

Glaub da werde ich noch die oder andere interessante Stunde mit verbringen.
